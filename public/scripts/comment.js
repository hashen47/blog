function handleEditBtnClick(e)
{
    const editBtn = e.target;
    const submitBtn = editBtn.nextElementSibling;
    const inputTag = editBtn.previousElementSibling;

    editBtn.style.display = "none";
    submitBtn.style.display = "inherit";
    inputTag.disabled = false;
    inputTag.focus();
}


function getInputKeyValuePairFromForm(f)
{
    const data = {};
    let form = new FormData(f);

    for (let [key, value] of form.entries()) {
        data[key] = value;
    }

    return data;
}


function handleEditFormSubmit(e)
{
    e.preventDefault();

    const data = getInputKeyValuePairFromForm(e.target);

    (async() => {
        const response = await fetch("/comment/update", {
            method : "PUT",
            headers : {
                "Content-type" : "application/json",
            },
            body : JSON.stringify(data)
        });

        const resp = await response.json();


        const commentTag = e.target.querySelector("#comment");

        if (resp.status == "success") {
            commentTag.setAttribute("preval", commentTag.value);
        } else {
            commentTag.value = commentTag.getAttribute("preval");
        }

        e.target.querySelector(".comment-edit-btn").style.display = "inherit";
        e.target.querySelector(".comment-save-btn").style.display = "none";
        commentTag.disabled = true;
    })();
}


function handleDeleteFormSubmit(e)
{
    e.preventDefault();

    const data = getInputKeyValuePairFromForm(e.target);

    (async() => {
        const response = await fetch("/comment/delete", {
            method : "DELETE",
            headers : {
                "Content-type" : "application/json",
            },
            body : JSON.stringify(data)
        });

        const resp = await response.json();

        if (resp.status == "success") {
            updateCommentCount(e.target);
            e.target.closest(".card").remove();
        }

    })();
}


function updateCommentCount(form)
{
    const parentDiv = form.closest(".form-group");
    const commentsCountDiv = parentDiv.querySelector(".comments-count");
    const commentsStoreDiv = parentDiv.querySelector(".comments-div");
    const commentsCount = commentsStoreDiv.children.length - 1;

    console.log(commentsCount);

    if (commentsCount <= 0) {
        commentsCountDiv.innerText = "No comments";
    } else {
        commentsCountDiv.innerText = `${commentsCount} comments`;
    }
}
