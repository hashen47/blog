<?php

use App\Http\Controllers\CommentController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\PostController;
use App\Http\Controllers\RegisterController;
use App\Http\Middleware\HasPermissionToEditComment;
use App\Http\Middleware\HasPermissionToEditPost;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get("/", [PostController::class, "all"])->name("home");


Route::prefix("auth")->group(function () {
    Route::get("/login", [LoginController::class, "index"])->name("login");

    Route::post("/login", [LoginController::class, "login"]);

    Route::get("/register", [RegisterController::class, "index"]);

    Route::post("/register", [RegisterController::class, "register"]);

    Route::get("/logout", function () {
        Auth::logout();
        return redirect()->route("login");
    });
});


Route::group(["prefix" => "posts", "middleware" => "auth"], function () {
    Route::get("/", [PostController::class, "index"]);

    Route::get("/{user_id}", [PostController::class, "show"])
        ->whereNumber("user_id")
        ->withoutMiddleware("auth");

    Route::get("/create", [PostController::class, "create"]);

    Route::post("/store", [PostController::class, "store"]);

    Route::get("/edit/{post_id}", [PostController::class, "edit"])
        ->whereNumber("post_id")
        ->middleware(HasPermissionToEditPost::class);

    Route::put("/update/{post_id}", [PostController::class, "update"])
        ->whereNumber("post_id")
        ->middleware(HasPermissionToEditPost::class);

    Route::delete("/destroy/{post_id}", [PostController::class, "destroy"])
        ->whereNumber("post_id")
        ->middleware(HasPermissionToEditPost::class);

    Route::post("/comment/{post_id}", [PostController::class, "comment"])
        ->whereNumber("post_id");
});


Route::group(["prefix" => "comment", "middleware" => HasPermissionToEditComment::class], function () {
    Route::put("/update", [CommentController::class, "update"]);

    Route::delete("/delete", [CommentController::class, "destroy"]);
});
