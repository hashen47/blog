@extends("base")


@section("title")
Create
@endsection


@section("body")
<div class="col">
    <form method="POST" action="/posts/store">
        @csrf
        <div class="form-group d-flex align-items-center justify-content-center">
            <h2
                style="font-size: 42px;"
            >Create Post</h2>
        </div>
        <div class="form-group">
            <h3 class="mt-3">Title</h3>
            <input
                class="form-control"
                name="title"
                type="text"
                value="{{ old('title') }}"
            >
        </div>
        <div class="form-group">
            <h3 class="mt-3">Content</h3>
            <textarea
                class="form-control"
                name="content"
                id=""
                cols="30"
                rows="10"
            >{{ old('content') }}</textarea>
        </div>
        <div class="form-group">
            <button
                type="submit"
                class="btn btn-primary form-control p-3 mt-3"
            >Create</button>
        </div>
    </form>
</div>
@endsection
