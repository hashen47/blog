@extends("base")


@section("title")
Update Post
@endsection


@section("body")
<div class="col">
    <form method="POST" action="/posts/update/{{ $post->id }}">
        @csrf
        @method("PUT")
        <div class="form-group d-flex align-items-center justify-content-center">
            <h2
                style="font-size: 42px;"
            >Update Post</h2>
        </div>
        <div class="form-group">
            <h3 class="mt-3">Title</h3>
            <input
                class="form-control"
                name="title"
                type="text"
                value="{{ old('title') ?? $post->title }}"
            >
        </div>
        <div class="form-group">
            <h3 class="mt-3">Content</h3>
            <textarea
                class="form-control"
                name="content"
                cols="30"
                rows="10"
            >{{ old('content') ?? $post->content }}</textarea>
        </div>
        <div class="form-group">
            <button
                type="submit"
                class="btn btn-success form-control p-3 mt-3"
            >Update</button>
        </div>
    </form>
</div>
@endsection
