@extends("base")

@php
    $currentUser = $user;

    if (Auth::user()) {
        if (Auth::user()->username == $user)
            $currentUser = "Your";
    }
@endphp

@section("title")
{{ $currentUser }} Posts
@endsection

@section("body")
<div class="col mt-4">
    <h1>
        {{ $currentUser . "'s "}} Posts
    </h1>
    @if($posts)
        @foreach($posts as $post)
            <x-post
                :post=$post
            />
        @endforeach
    @endif
</div>
@endsection

@push("scripts")
<script src="{{ asset('scripts/comment.js') }}"></script>
@endpush
