@extends("base")

@section("title")
    @if ($all)
        All Posts
    @else
        Your's Posts
    @endif
@endsection

@section("body")
<div class="col mt-4">
    <h1>
        @if ($all)
            All Posts
        @else
            Your's Posts
        @endif
    </h1>
    @if($posts)
        @foreach($posts as $post)
            <x-post
                :post=$post
            />
        @endforeach
    @endif
</div>
@endsection

@push("scripts")
<script src="{{ asset('scripts/comment.js') }}"></script>
@endpush
