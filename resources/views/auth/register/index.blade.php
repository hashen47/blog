@extends("base")

@section("title")
Register
@endsection

@section("body")
<div class="col d-flex align-items-center justify-content-center mt-5">
    <div class="col col-md-8">
        <form action="/auth/register" method="POST">
            @csrf
            <div class="form-group d-flex justify-content-center align-items-center">
                <h1
                    class="mb-4"
                    @style([
                        "font-size: 46px"
                    ])
                >
                    Register
                </h1>
            </div>
            <div class="form-group">
                <h4>
                    Username
                </h4>
                <input
                    type="text"
                    class="form-control mb-4"
                    name="username"
                    value="{{ old('username') }}"
                >
            </div>
            <div class="form-group">
                <h4>Password</h4>
                <input
                    type="password"
                    class="form-control mb-4"
                    name="password"
                >
            </div>
            <div class="form-group">
                <h4>Re-Password</h4>
                <input
                    type="password"
                    class="form-control mb-5"
                    name="repassword"
                >
            </div>
            <div class="form-group">
                <input
                    type="submit"
                    class="btn btn-primary form-control p-3"
                    value="Register"
                >
            </div>
            <div class="form-group d-flex justify-content-center align-items-center mt-3">
                <span
                    class="me-1"
                >Already have an account?</span>
                <a href="/auth/login">Login</a>
            </div>
        </form>
    </div>
</div>
@endsection
