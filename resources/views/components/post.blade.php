<div class="card mt-4 mb-5">
    <div class="card-header d-flex justify-content-between">
        <div class="col">
            <h3>
                <a href="/posts/{{ $post->user_id }}">
                        {{ $post->username }}
                </a>
            </h3>
        </div>
        <div class="col d-flex justify-content-end">
            @if (Auth::user())
                @if (Auth::user()->id == $post->user_id)
                    <form
                        method="GET"
                        action="/posts/edit/{{ $post->id }}"
                    >
                        <button
                            type="submit"
                            class="btn btn-success"
                        >Edit</button>
                    </form>

                    <form
                        method="POST"
                        action="/posts/destroy/{{ $post->id }}"
                    >
                        @csrf
                        @method("DELETE")
                        <button
                            type="submit"
                            class="btn btn-danger ms-1"
                        >Delete</button>
                    </form>
                @endif
            @endif
        </div>
    </div>
    <div class="card-body">
        <h5 class="card-title">
            {{ $post->title }}
        </h5>
        <p class="card-text">
            {{ $post->content }}
        </p>
    </div>
        <div class="card-body">
            @if (Auth::user())
                <form method="POST" action="/posts/comment/{{ $post->id }}">
                    @csrf
                    <div class="input-group mb-3">
                        <input
                            type="text"
                            class="form-control"
                            name="comment"
                            placeholder="Enter your comment.."
                        >

                        <button
                            class="btn btn-primary"
                            type="submit"
                        >Comment</button>
                    </div>
                </form>
            @endif

            <div class="form-group">
                <p>
                     <a data-bs-toggle="collapse" href="#comments{{ $post->id }}" class="comments-count" role="button" aria-expanded="false" aria-controls="comments">
                         @if(count($post->comments) > 0)
                            {{ count($post->comments) }} comments
                         @else
                            No comments
                         @endif
                     </a>
                </p>

                @if(count($post->comments) > 0)
                    <div class="collapse" id="comments{{ $post->id }}">
                         <div class="card card-body comments-div">
                             @foreach($post->comments as $comment)
                                 <x-comment
                                    :$comment
                                 />
                             @endforeach
                         </div>
                    </div>
                @endif
            </div>
        </div>
    <div class="card-footer text-muted">
        {{ now()->diffInDays($post->created_at) }} days ago
    </div>
</div>
