@php
    function setActiveToCurrentPath(string $path) : string
    {
        $result = "nav-link ";

        if (Request::path() == $path) {
            $result .= "active";
        }

        return $result;
    }
@endphp

<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <div class="container-fluid">
        <a class="navbar-brand" href="/posts">
            {{ $navBrand }}
        </a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
            <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                <li class="nav-item">
                    <a
                        aria-current="page"
                        class="{{ setActiveToCurrentPath('/') }}"
                        href="/"
                    >Home</a>
                </li>

                @if(Auth::user())
                    <li class="nav-item">
                        <a
                            class="{{ setActiveToCurrentPath('posts') }}"
                            href="/posts"
                        >Posts</a>
                    </li>
                    <li class="nav-item">
                        <a
                            class="{{ setActiveToCurrentPath('posts/create') }}"
                            href="/posts/create"
                        >Create</a>
                    </li>
                @endif
            </ul>
            <ul class="navbar-nav mb-2 mb-lg-0">
                @if (Auth::user())
                    <li class="nav-item">
                        <a
                            class="{{ setActiveToCurrentPath('auth/logout') }}"
                            href="/auth/logout"
                        >Logout</a>
                    </li>
                @else
                    <li class="nav-item">
                        <a
                            class="{{ setActiveToCurrentPath('auth/login') }}"
                            href="/auth/login"
                        >Login</a>
                    </li>
                    <li class="nav-item">
                        <a
                            class="{{ setActiveToCurrentPath('auth/register') }}"
                            href="/auth/register"
                        >Register</a>
                    </li>
                @endif
            </ul>
        </div>
    </div>
</nav>

