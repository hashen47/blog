@php
    $ownComment = Auth::id() == $comment->user_id;
@endphp

<div class="card mt-3 mb-3">
    <div
        @class([
            "card-body",
            "bg-primary-subtle" => $ownComment,
        ])
    >
        <div class="col d-flex justify-content-between align-items-center">
            <h6>{{ $comment->username }}</h6>
            <h6>{{ $comment->created_at }}</h6>
        </div>

        <div class="col d-flex">
            <form
                action="/comment/update"
                class="col d-flex"
                method="POST"
                onsubmit="handleEditFormSubmit(event)"
            >
                @csrf
                @method("PUT")
                <input
                    type="hidden"
                    name="comment_id"
                    value="{{ $comment->id }}"
                >
                <input
                    type="text"
                    class="form-control"
                    name="comment"
                    id="comment"
                    preval="{{ $comment->comment }}"
                    value="{{ $comment->comment }}"
                    disabled
                >

                @if (
                    Auth::user()
                    && Auth::id() == $comment->user_id
                )
                    <button
                        type="button"
                        class="btn btn-outline-primary comment-edit-btn"
                        onclick="handleEditBtnClick(event);"
                    >Edit</button>
                    <button
                        type="submit"
                        name="save"
                        class="btn btn-outline-success comment-save-btn"
                        style="display: none;"
                    >Save</button>
                @endif

            </form>

            <form
                action="/comment/delete"
                method="POST"
                onsubmit="handleDeleteFormSubmit(event)"
            >
                @csrf
                @method("DELETE")
                <input
                    type="hidden"
                    name="comment_id"
                    value="{{ $comment->id }}"
                >

                @if (
                    Auth::user()
                    && Auth::id() == $comment->user_id
                )
                    <button
                        comment_id="{{ $comment->id }}"
                        type="submit"
                        class="btn btn-outline-danger"
                    >Delete</button>
                @endif

            </form>
        </div>

    </div>
</div>
