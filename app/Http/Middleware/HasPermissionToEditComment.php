<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class HasPermissionToEditComment
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): JsonResponse | Response
    {
        $comment = DB::table("comments")
            ->where("id", $request->input("comment_id"))
            ->where("user_id", Auth::id())
            ->first();

        if ($comment == null)
            return response()
                ->json([
                    "status" => "error",
                    "msg" => "You don't have permission to edit this comment"
                ]);

        return $next($request);
    }
}
