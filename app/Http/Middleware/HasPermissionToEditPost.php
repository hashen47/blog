<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpFoundation\Response;

class HasPermissionToEditPost
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {
        $post = DB::table("posts")
            ->where("id", "=", $request->post_id)
            ->where("user_id", "=", Auth::id())
            ->first();

        if ($post == null)
            return redirect("/")
                ->withErrors([
                    "post_id" => "You don't have permission"
                ]);

        return $next($request);
    }
}
