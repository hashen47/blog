<?php

namespace App\Http\Controllers;

use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    public function index() : View
    {
        return view("auth.register.index");
    }


    public function register(Request $request) : RedirectResponse
    {
        // validate
        $validator = Validator::make(
            $request->all(),
            [
                "username" => ['required', 'unique:users', 'min:5', 'max:255'],
                "password" => ['required', 'min:8', 'max:255'],
                "repassword" => ['required', 'min:8', 'max:255', 'same:password'],
            ],
            [],
            [
                "username" => "Username",
                "password" => "Password",
                "repassword" => "RePassword",
            ]
        );

        if ($validator->stopOnFirstFailure()->fails()) {
            return redirect("/auth/register")
                ->withErrors($validator)
                ->withInput([
                    "username" => $request->input("username") ?? ''
                ]);
        }


        // add user to database
        $validatedInputs = $validator->validated();

        DB::table("users")->insert([
            "username" => $validatedInputs["username"],
            "password" => Hash::make($validatedInputs["password"]),
            "created_at" => DB::raw("CURRENT_TIMESTAMP"),
        ]);

        return redirect("/auth/register")
            ->withInput([
                "status" => "success",
                "msg" => "Successfully Registered ther User"
            ]);
    }
}
