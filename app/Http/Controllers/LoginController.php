<?php

namespace App\Http\Controllers;

use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class LoginController extends Controller
{
    public function index() : View|RedirectResponse
    {
        if (Auth::user()) {
            return redirect("/");
        }
        return view("auth.login.index");
    }


    public function login(Request $request) : RedirectResponse
    {
        $validator = Validator::make(
            $request->all(),
            [
                "username" => "required",
                "password" => "required",
            ],
            [],
            [
                "username" => "Username",
                "password" => "Password",
            ]
        );

        if ($validator->stopOnFirstFailure()->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput([
                    "username" => $request->input("username") ?? ''
                ]);
        }

        if (Auth::attempt($validator->validated())) {
            $request->session()->regenerate();
            return redirect("/")
                ->withInput([
                    "status" => "success",
                    "msg" => "Successfully logged in the user",
                ]);
        }

        return back()
            ->withErrors([
                "username" => "Username and Password combination is invalid"
            ]);

    }
}
