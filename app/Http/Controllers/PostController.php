<?php

namespace App\Http\Controllers;

use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class PostController extends Controller
{
    /**
     * show all posts of all users
    */
    public function all() : View|RedirectResponse
    {
        $posts = DB::table("posts")
            ->join("users", "users.id", "=", "posts.user_id")
            ->select("posts.*", "users.username")
            ->orderBy("posts.id", "desc")
            ->get();

        for($i = 0; $i < count($posts); $i++) {
            $posts[$i]->comments =
                DB::table("comments")
                ->join("users", "users.id", "comments.user_id")
                ->where("post_id", $posts[$i]->id)
                ->select("users.username", "comments.*")
                ->orderBy("comments.created_at", "desc")
                ->get() ?? [];
        }

        if (count($posts) == 0) {
            if (Auth::user()) {
                return redirect("/posts/create")
                    ->withInput([
                        "status" => "warning",
                        "msg" => "No Posts Yet, Before access create some posts.."
                    ]);
            }
            return redirect("/auth/login")
                ->withInput([
                    "status" => "warning",
                    "msg" => "No Posts Yet, Before access login and create some posts..."
                ]);
        }

        return view("posts.index", [
            "all" => true,
            "posts" => $posts,
        ]);
    }

    /**
     * show only current users's posts
    */
    public function index() : View|RedirectResponse
    {
        $posts = DB::table("posts")
            ->join("users", "users.id", "=", "posts.user_id")
            ->select("posts.*", "users.username")
            ->where("posts.user_id", Auth::id())
            ->orderBy("posts.id", "desc")
            ->get();

        for($i = 0; $i < count($posts); $i++) {
            $posts[$i]->comments =
                DB::table("comments")
                ->join("users", "users.id", "comments.user_id")
                ->where("post_id", $posts[$i]->id)
                ->select("users.username", "comments.*")
                ->orderBy("comments.created_at", "desc")
                ->get() ?? [];
        }

        if (count($posts) == 0) {
            if (Auth::user()) {
                return redirect("/posts/create")
                    ->withInput([
                        "status" => "warning",
                        "msg" => "No Posts Yet, Before access create some posts.."
                    ]);
            }
            return redirect("/auth/login")
                ->withInput([
                    "status" => "warning",
                    "msg" => "No Posts Yet, Before access login and create some posts..."
                ]);
        }

        return view("posts.index", [
            "all" => false,
            "posts" => $posts
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create() : View
    {
        return view("posts.create");
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request) : RedirectResponse
    {
        $validator = Validator::make(
            $request->all(),
            [
                "title" => "required|min:10|max:255",
                "content" => "required|min:10|max:400",
            ],
            [],
            [
                "title" => "Title",
                "content" => "Content",
            ]
        );


        if ($validator->stopOnFirstFailure()->fails()) {
            return redirect("/posts/create")
                ->withErrors($validator)
                ->withInput([
                    "title" => $request->input("title") ?? "",
                    "content" => $request->input("content") ?? "",
                ]);
        }


        $validatedData = $validator->validated();
        DB::table("posts")
            ->insert([
                "title" => $validatedData["title"],
                "content" => $validatedData["content"],
                "user_id" => Auth::id(),
                "created_at" => now(),
            ]);


        return redirect("/posts/create")
            ->withInput([
                "status" => "success",
                "msg" => "Successfully created the Post",
            ]);
    }

    /**
     * Display the specified resource.
     */
    public function show(int $userId) : View|RedirectResponse
    {
        $user = DB::table("users")
            ->where("id", $userId)
            ->first();

        if ($user == null)
            return redirect("/");

        if ($user->id == Auth::id())
            return redirect("/posts");

        $posts = DB::table("posts")
            ->where("user_id", $userId)
            ->join("users", "users.id", "=", "posts.user_id")
            ->select("users.username", "posts.*")
            ->get();

        for($i = 0; $i < count($posts); $i++) {
            $posts[$i]->comments =
                DB::table("comments")
                ->join("users", "users.id", "comments.user_id")
                ->where("post_id", $posts[$i]->id)
                ->select("users.username", "comments.*")
                ->orderBy("comments.created_at", "desc")
                ->get() ?? [];
        }

        return view("posts.show", [
            "posts" => $posts,
            "user" => $user->username,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(int $postId) : View
    {
        $post = DB::table("posts")
            ->where("id", $postId)
            ->first();

        return view("posts.edit", [
            "post" => $post
        ]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, int $postId) : RedirectResponse
    {
        $validator = Validator::make(
            $request->all(),
            [
                "title" => "required|min:10|max:255",
                "content" => "required|min:10|max:400",
            ],
            [],
            [
                "title" => "Title",
                "content" => "Content",
            ]
        );

        if ($validator->stopOnFirstFailure()->fails()) {
            return redirect("/posts/edit/$postId")
                ->withErrors($validator)
                ->withInput([
                    "title" => $request->input("title") ?? "",
                    "content" => $request->input("content") ?? "",
                ]);
        }

        $validatedData = $validator->validated();
        DB::table("posts")
            ->where("id", $postId)
            ->update([
                "title" => $validatedData["title"],
                "content" => $validatedData["content"],
                "updated_at" => now(),
            ]);

        return redirect("/posts/edit/$postId")
            ->withInput([
                "status" => "success",
                "msg" => "Successfully updated the Post"
            ]);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(int $postId) : RedirectResponse
    {
        DB::table("posts")
            ->where("id", $postId)
            ->delete();

        return redirect("/posts")
            ->withInput([
                "status" => "success",
                "msg" => "Successfully deleted the Post",
            ]);
    }

    /**
     * Add comments to posts
     */
    public function comment(Request $request, int $postId) : RedirectResponse
    {
        $post = DB::table("posts")
            ->where("id", $postId)
            ->first();

        if ($post == null) {
            return back();
        }

        $validator = Validator::make(
            $request->all(),
            [
                "comment" => "required",
            ],
        );

        if ($validator->stopOnFirstFailure()->fails()) {
            return back()
                ->withInput([
                    "comment" => $request->input("comment") ?? ""
                ]);
        }

        $validatedData = $validator->validated();

        DB::table("comments")
            ->insert([
                "post_id" => $postId,
                "user_id" => Auth::id(),
                "comment" => $validatedData["comment"],
                "created_at" => now(),
            ]);

        return back();
    }
}
