<?php

namespace App\Http\Controllers;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class CommentController extends Controller
{
    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request, int $postId) : RedirectResponse
    {
        $post = DB::table("posts")
            ->where("id", $postId)
            ->first();

        if ($post == null) {
            return back();
        }

        $validator = Validator::make(
            $request->all(),
            [
                "comment" => "required",
            ],
        );

        if ($validator->stopOnFirstFailure()->fails()) {
            return back();
        }

        $validatedData = $validator->validated();

        DB::table("comments")
            ->insert([
                "post_id" => $postId,
                "user_id" => Auth::id(),
                "comment" => $validatedData["comment"],
                "created_at" => now(),
            ]);

        return back();
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request) : JsonResponse
    {
        /**
         * comment_id validation in the HasPermissionToEditComment middleware
         */
        $validator = Validator::make(
            $request->all(),
            [
                "comment" => "required",
            ]
        );

        if ($validator->stopOnFirstFailure()->fails()) {
            return response()
                ->json([
                    "status" => "error",
                    "msg" => "Comment can't be empty",
                ]);
        }

        DB::table("comments")
            ->where("id", $request->input("comment_id"))
            ->update([
                "comment" => $request->input("comment"),
                "updated_at" => now()
            ]);

        return response()
            ->json([
                "status" => "success"
            ]);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Request $request) : JsonResponse
    {
        /**
         * comment validation in the HasPermissionToEditComment middleware
         */
        DB::table("comments")
            ->where("id", $request->input("comment_id"))
            ->delete();

        return response()
            ->json([
                "status" => "success"
            ]);
    }
}
