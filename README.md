# Blog


## Content
- [Blog](#blog)
  - [Content](#content)
  - [About](#about)
  - [Setup](#setup)
  - [License](#license)


## About
- Simple CRUD app created using laravel framework (laravel 10)
- Also using database connection as mysql


## Dependencies
- node (version 18 or above)
- npm
- php 
- mysql server
- composer (php package manager)


## Setup
- First clone the repo
    ```
    git clone https://gitlab.com/hashen47/blog.git
    ```

- Then go to the repo folder
    ```
    cd blog
    ```

- Then run below command to install composer dependencies 
    ```
    composer install
    ```

- Then cp .env.example file to .env file
    ```
    cp .env.example .env
    ```

- Then edit the .env file mysql connection credentials (replace them with your database credentials)

    ![screenshot1](images/screenshot1.png)


- Then run below command to generate keys
    ```
    php artisan key:generate
    ```

- Then to migrate database tables run command mention in below
    ```
    php artisan migrate
    ```

- Then start the server using below command
    ```
    php artisan serve
    ```

- Then go to the link that show in your terminal

    ![screenshot2](images/screenshot2.png)


## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
